import CarRunner.Car;

public class Main {
    public static void main(String[] args){
        Car test = new Car();
        test.start();
        System.out.println(test.isEngineOn());
    }
}
